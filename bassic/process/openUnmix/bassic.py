import librosa
import soundfile as sf
from playsound import playsound
from .test import separate


def bassFilter(path):
    audio, rate = librosa.load(
        path,
        sr=44100,
        offset=0,
        mono=False
    )

    for i in range(6):
        estimates = separate(
            audio=audio.T,
            targets=['bass'],
            residual_model=False,
            niter=1
        )

        for target, estimate in estimates.items():
            if(target == 'bass'):
                audio = estimate.T

    for target, estimate in estimates.items():
        if(target == 'bass'):
            audio = estimate
            sf.write(path, estimate, 44100, subtype='PCM_16')

    return audio
