import seaborn
import numpy
import scipy
import librosa

# Function for retrieving the frequency by using autocorrelation algorithm


def freq(n, samples, sr, data):
    n0 = samples[n]
    n1 = samples[n+1]
    f0 = data[n0:n1]

    r = librosa.autocorrelate(f0)
    i_min = sr/400.0
    i_max = sr/40.0
    r[:int(i_min)] = 0
    r[int(i_max):] = 0

    i = r.argmax()
    freq = float(sr)/i

    return freq


def frequency(filtered):

    frequency = []

    x, sr = librosa.load(filtered)
    hop_length = 250

    onset_samples = librosa.onset.onset_detect(x, sr=sr, units='samples', hop_length=hop_length,
                                               backtrack=False, pre_max=20, post_max=20, pre_avg=100, post_avg=100, delta=0.114, wait=0)

    for index in range(len(onset_samples)-1):
        init_freq = librosa.hz_to_note(freq(index, onset_samples, sr, x))

        if not frequency:
            frequency.append(init_freq)
        if frequency and frequency[len(frequency)-1] != init_freq:
            frequency.append(init_freq)

    return frequency


def note(freq):
    note = []

    for i in freq:
        note.append(librosa.note_to_hz(i))

    return note
