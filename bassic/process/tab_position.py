def note_to_tab(note):
    tab = []
    for x in note:
        if(x == "A1"):

            tab.append({
                "string": 4,
                "fret": 5
            })

        if(x == "A#1"):

            tab.append({
                "string": 4,
                "fret": 6,
            })

        if(x == "B1"):

            tab.append({
                "string": 4,
                "fret": 7,
            })

        if(x == "E1"):

            tab.append({
                "string": 4,
                "fret": 0,
            })

        if(x == "F1"):

            tab.append({
                "string": 4,
                "fret": 1,
            })

        if(x == "F#1"):

            tab.append({
                "string": 4,
                "fret": 2,
            })

        if(x == "G1"):

            tab.append({
                "string": 4,
                "fret": 3,
            })

        if(x == "G#1"):

            tab.append({
                "string": 4,
                "fret": 4,
            })

        if(x == "A2"):

            tab.append({
                "string": 2,
                "fret": 7,
            })

        if(x == "A#2"):

            tab.append({
                "string": 2,
                "fret": 8,
            })

        if(x == "B2"):

            tab.append({
                "string": 2,
                "fret": 9,
            })

        if(x == "C2"):

            tab.append({
                "string": 3,
                "fret": 3,
            })

        if(x == "C#2"):

            tab.append({
                "string": 3,
                "fret": 4,
            })

        if(x == "D2"):

            tab.append({
                "string": 3,
                "fret": 5,
            })

        if(x == "D#2"):

            tab.append({
                "string": 2,
                "fret": 1,
            })

        if(x == "E2"):

            tab.append({
                "string": 2,
                "fret": 2,
            })
        #     return Position

        if(x == "F2"):

            tab.append({
                "string": 2,
                "fret": 3,
            })

        if(x == "F#2"):

            tab.append({
                "string": 2,
                "fret": 4,
            })

        if(x == "G2"):

            tab.append({
                "string": 2,
                "fret": 5,
            })

        if(x == "G#2"):

            tab.append({
                "string": 2,
                "fret": 6,
            })

        if(x == "A3"):

            tab.append({
                "string": 2,
                "fret": 19,
            })

        if(x == "A#3"):

            tab.append({
                "string": 1,
                "fret": 15,
            })

        if(x == "B3"):

            tab.append({
                "string": 1,
                "fret": 16,
            })
        #     return Position

        if(x == "C3"):

            tab.append({
                "string": 2,
                "fret": 10,
            })

        if(x == "C#3"):

            tab.append({
                "string": 2,
                "fret": 11,
            })
        if(x == "D3"):

            tab.append({
                "string": 1,
                "fret": 7,
            })

        if(x == "D#3"):

            tab.append({
                "string": 1,
                "fret": 8,
            })

        if(x == "E3"):

            tab.append({
                "string": 1,
                "fret": 9,
            })

        if(x == "F3"):

            tab.append({
                "string": 1,
                "fret": 10,
            })

        if(x == "F#3"):

            tab.append({
                "string": 1,
                "fret": 11,
            })

        if(x == "G3"):

            tab.append({
                "string": 1,
                "fret": 12,
            })

        if(x == "G#3"):

            tab.append({
                "string": 1,
                "fret": 13,
            })

        if(x == "C4"):

            tab.append({
                "string": 1,
                "fret": 117,
            })

        if(x == "C#4"):

            tab.append({
                "string": 1,
                "fret": 18,
            })

        if(x == "D4"):

            tab.append({
                "string": 1,
                "fret": 19,
            })

        if(x == "D#4"):

            tab.append({
                "string": 1,
                "fret": 20,
            })

        if(x == "E4"):

            tab.append({
                "string": 1,
                "fret": 21,
            })

        if(x == "F4"):

            tab.append({
                "string": 1,
                "fret": 22,
            })

        if(x == "F#4"):

            tab.append({
                "string": 1,
                "fret": 23,
            })

        if(x == "G4"):
            tab.append({
                "string": 1,
                "fret": 24,
            })

    return tab
