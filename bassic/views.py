from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views import View
from django.core.files.storage import FileSystemStorage

from .process.pitch_detect import frequency, note
from .process.tab_position import note_to_tab
from .process.openUnmix.bassic import bassFilter

from tinytag import TinyTag


class home(View):
    template_name = 'home.html'

    def get(self, request):
        return render(request, self.template_name)

    def post(self, request, *args, **kwargs):
        audioFile = request.FILES['audioForm']
        fs = FileSystemStorage()
        filename = fs.save(audioFile.name, audioFile)



        return redirect('tabs', filename=filename)


class tab(View):
    
    def getcontext(self, filename):
        path = f"media/{filename.replace('%20', ' ')}"
        print(path)
        bassFilter(path)  # call to process the audio separation
        Freq = frequency(path)
        tag = TinyTag.get(path)
        context = {
            'uploaded_file_url': f'/{path}',
            'frequency': Freq,
            'notes': note(Freq),
            'tab_position': note_to_tab(Freq),
            'tags': {
                'title': tag.title,
                'filename': filename.replace('.wav', ''),
                'artist': tag.artist
            }

        }

        return context

    def get(self, request, filename):
        context = self.getcontext(filename)

        return render(request, 'tabs.html', context)
