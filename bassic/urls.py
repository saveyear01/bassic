from django.contrib import admin
from django.urls import include, path

from .views import home, tab

urlpatterns = [
    path('', home.as_view()),
    path('tab/<filename>', tab.as_view(), name='tabs')

]
