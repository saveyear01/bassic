$(document).ready(function() {
  let fileEl = $(".form_input");
  let submitEl = $(".custom-file__submit");
  let errorCard = $(".error-card");
  let loaderEl = $(".loader");

  $('input[type="file"]').change(function() {
    if ($(this).val()) {
      let filename = $(this)
        .val()
        .replace(/^.*[\\\/]/, "");
      if (filename.endsWith(".wav")) {
        $(".custom-file__name").html(filename);
        fileEl.addClass("hidden");
        submitEl.removeClass("hidden");
      } else {
        errorCard.removeClass("hidden");
        setTimeout(function() {
          errorCard.addClass("hidden");
        }, 3000);
      }
    }
  });

  $(".submit-button").click(function() {
    loaderEl.removeClass("hidden");
    submitEl.addClass("hidden");
  });
});
