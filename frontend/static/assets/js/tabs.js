let data = fret_data;
let splitData = [];

function pushData(index, object) {
  // splitData[index].push(object);
  // console.log(index);
  // splitData.push(object);
}

$(document).ready(function() {
  var startIndex = 0;
  var endIndex = 20;
  for (let index = 0; index < data.length / 20; index++) {
    splitData[index] = data.slice(startIndex, endIndex);
    startIndex = startIndex + 20;
    endIndex = endIndex + 20;
  }
  let elContainer = $(this).find(".tab-container");
  let newElem = $(elContainer)
    .find(".tab-graph")
    .clone();

  newElem.map(function() {
    $(this).removeClass("hidden");
  });

  for (const line of splitData) {
    let tabGraphContainer = newElem.clone();
    for (const frets of line) {
      let tabStringEl = newElem.find(".tab-string").clone();
      tabStringEl.map(function() {
        $(this).removeClass("hidden");
        $(this)
          .find("span")
          .text(frets.fret);
        if (frets.string === 1) {
          $(this).addClass("tab-string-1");
        }
        if (frets.string === 2) {
          $(this).addClass("tab-string-2");
        }
        if (frets.string === 3) {
          $(this).addClass("tab-string-3");
        }
        if (frets.string === 4) {
          $(this).addClass("tab-string-4");
        }
      });
      tabGraphContainer.append(tabStringEl);
    }
    elContainer.append(tabGraphContainer);
  }
});
